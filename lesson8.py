'''
Created on Jun 10, 2014

@author: hauyeung
'''
from tkinter import *

root = Tk()
root.wm_title("Frame Layout")
root.grid()
for r in range(6):
    root.rowconfigure(r, weight=1)
for c in range(5):
    root.columnconfigure(c, weight=1)
    Button(root, text='Button {0}'.format(c)).grid(row = 6, column = c, sticky = E+W)
frame1 = Frame(root,bg='black')
frame1.grid(row = 0, column = 0, rowspan = 3, columnspan = 2, sticky = W+E+N+S)
frame2 = Frame(root,bg='white')
frame2.grid(row = 3, column = 0, rowspan = 3, columnspan = 2, sticky = W+E+N+S)
frame3 = Frame(root,bg='green')
frame3.grid(row = 0, column = 2, rowspan = 6, columnspan = 3, sticky = W+E+N+S)




root.mainloop()